/*
 * Sievely blocks
 * Copyright 2016 Daniel Silverstone <daniel.silverstone@codethink.co.uk>
 *
 * See COPYING
 */

'use strict';

goog.provide('Sievely.Blocks')

goog.require('Blockly.Blocks')

Sievely.Blocks.STATEMENT_HUE = 120;
Sievely.Blocks.COMMENT_HUE = 165;
Sievely.Blocks.BOOLEAN_HUE = 330;
Sievely.Blocks.ACTION_HUE = 230;
Sievely.Blocks.ADJUSTOR_HUE = 20;

Sievely.Blocks.HELP_URL = "https://somewhere/sievely";

Blockly.Blocks['sievely_start'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("when a message arrives:");
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.STATEMENT_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_comment'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("/*")
        .appendField(new Blockly.FieldTextInput("comment"), "COMMENT")
        .appendField("*/");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.COMMENT_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

/* Matches */

Blockly.Blocks['sievely_listcheck'] = {
  init: function() {
    this.appendValueInput("LISTNAME")
        .setCheck("String")
        .appendField("the message is from mailing list");
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_isfrom'] = {
  init: function() {
    this.appendValueInput("ADDR")
        .setCheck("String")
        .appendField("the message is from");
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_envelope_detail'] = {
  init: function() {
    this.appendValueInput("DETAIL")
        .setCheck("String")
        .appendField("the addressee suffix is");
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_header_test'] = {
  init: function() {
    this.appendValueInput("HEADER")
        .setCheck("Header");
    this.appendValueInput("CONTENT")
        .setCheck("String")
        .appendField(new Blockly.FieldDropdown([["contains", ":contains"], ["is", ":is"], ["matches", ":matches"]]), "MATCH");
    this.setInputsInline(true);
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_header'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("the")
        .appendField(new Blockly.FieldTextInput("Subject"), "HEADER")
        .appendField("header");
    this.setOutput(true, "Header");
    this.setColour(Blockly.Blocks.texts.HUE);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['sievely_size'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("the message is")
        .appendField(new Blockly.FieldDropdown([["at least", ":over"], ["at most", ":under"]]), "TEST")
        .appendField(new Blockly.FieldTextInput("10K"), "SIZE")
        .appendField("in size");
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  },
  onchange: function() {
    var inputvalue = this.getFieldValue('SIZE');
    var sanitised = Sievely.sanitiseSieveNumber(inputvalue);
    if (inputvalue == sanitised) {
      this.setWarningText(null);
    } else {
      this.setWarningText("Input '" + inputvalue + "' interpreted as '" + sanitised + "'");
    }
  }
};

/* Actions */

Blockly.Blocks['sievely_fileinto'] = {
  init: function() {
    this.appendValueInput("MAILBOX")
        .setCheck(["String","Folder"])
        .appendField("file into");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.ACTION_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_filecopyinto'] = {
  init: function() {
    this.appendValueInput("MAILBOX")
        .setCheck(["String","Folder"])
        .appendField("file a copy into");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.ACTION_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_reject'] = {
  init: function() {
    this.appendValueInput("REASON")
        .setCheck("String")
        .appendField("reject saying");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.ACTION_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_stop'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("stop processing here");
    this.setPreviousStatement(true, null);
    this.setColour(Sievely.Blocks.ACTION_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_discard'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("discard message and stop processing here");
    this.setPreviousStatement(true, null);
    this.setColour(Sievely.Blocks.ACTION_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

/* Adjustor blocks */

Blockly.Blocks['sievely_mark'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mark message as")
        .appendField(new Blockly.FieldDropdown([["already read", "Seen"], ["unread", "!Seen"], ["high priority", "Flagged"], ["normal priority", "!Flagged"], ["already answered", "Answered"], ["unanswered", "!Answered"]]), "MARK");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.ADJUSTOR_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

/* Special groups */

Blockly.Blocks['sievely_group'] = {
  init: function() {
    this.appendValueInput("COND0")
        .setCheck("Boolean")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldDropdown([["any of", "ANYOF"], ["all of", "ALLOF"]]), "KIND");
    this.appendValueInput("COND1")
        .setCheck("Boolean")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("or");
    this.setOutput(true, "Boolean");
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
    this.setMutator(new Blockly.Mutator(['sievely_group_extra']));
    this.extras_ = 0;
  },
  mutationToDom: function() {
    if (this.extras_ == 0) {
      return null;
    }
    var container = document.createElement('mutation');
    container.setAttribute('extras', this.extras_);
    return container;
  },
  domToMutation: function(xmlElement) {
    this.extras_ = parseInt(xmlElement.getAttribute('extras'), 10) || 0;
    this.updateShape_();
  },
  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('sievely_group_group');
    containerBlock.initSvg();
    var connection = containerBlock.nextConnection;
    for (var i = 1; i <= this.extras_; i++) {
      var extraBlock = workspace.newBlock('sievely_group_extra');
      extraBlock.initSvg();
      connection.connect(extraBlock.previousConnection);
      connection = extraBlock.nextConnection;
    }
    return containerBlock;
  },
  compose: function(containerBlock) {
    var clauseBlock = containerBlock.nextConnection.targetBlock();
    // Count number of inputs.
    this.extras_ = 0;
    var valueConnections = [null];
    while (clauseBlock) {
      switch (clauseBlock.type) {
        case 'sievely_group_extra':
          this.extras_++;
          valueConnections.push(clauseBlock.valueConnection_);
          break;
        default:
          throw 'Unknown block type.';
      }
      clauseBlock = clauseBlock.nextConnection &&
          clauseBlock.nextConnection.targetBlock();
    }
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 1; i <= this.extras_; i++) {
      Blockly.Mutator.reconnect(valueConnections[i], this, 'COND' + (i+1));
    }
  },
  saveConnections: function(containerBlock) {
    var clauseBlock = containerBlock.nextConnection.targetBlock();
    var i = 2;
    while (clauseBlock) {
      switch (clauseBlock.type) {
        case 'sievely_group_extra':
	  var cond = this.getInput('COND' + i)
	  clauseBlock.valueConnection_ =
	      cond && cond.connection.targetConnection;
          i++;
          break;
        default:
          throw 'Unknown block type.';
      }
      clauseBlock = clauseBlock.nextConnection &&
          clauseBlock.nextConnection.targetBlock();
    }
  },
  updateShape_: function() {
    // Delete everything.
    var i = 2;
    while (this.getInput('COND' + i)) {
      this.removeInput('COND' + i);
      i++;
    }
    // Rebuild block.
    for (var i = 1; i <= this.extras_; i++) {
      this.appendValueInput('COND' + (i+1))
          .setCheck('Boolean')
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField(this.andor_);
    }
  },
  onchange: function(changeEvent) {
    var kind = this.getField('KIND').getValue();
    this.andor_ = (kind == "ANYOF") ? "or" : "and";
    for (var i = 1; i < this.inputList.length; i++) {
      var f = this.inputList[i].fieldRow.pop();
      if (f) {
	f.dispose();
      }
      this.inputList[i].appendField(this.andor_);
    }
  }
};

Blockly.Blocks['sievely_group_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("group");
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

Blockly.Blocks['sievely_group_extra'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("additional test");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Sievely.Blocks.BOOLEAN_HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};

/* Live folder list */

function livesievelyfolderlist() {
  var ret = [];
  var folders = Sievely.allSievelyFolders;
  for (var i = 0; i < folders.length; i++) {
    ret[i] = [folders[i], folders[i]];
  }
  return ret;
}

Blockly.Blocks['sievely_folder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(livesievelyfolderlist), "FOLDER");
    this.setOutput(true, "Folder");
    this.setColour(Blockly.Blocks.texts.HUE);
    this.setTooltip('');
    this.setHelpUrl(Sievely.Blocks.HELP_URL);
  }
};
