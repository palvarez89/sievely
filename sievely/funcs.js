/*
 * Sievely functions
 * Copyright 2016 Daniel Silverstone <daniel.silverstone@codethink.co.uk>
 *
 * See COPYING
 */

'use strict';

goog.provide("Sievely")

goog.require("Blockly.Blocks")

Sievely.allSievelyFolders = ["INBOX"] // default to "INBOX" just in case

Sievely.state = "UNKNOWN"
Sievely.marker = "# GENERATOR=SIEVELY\n"
Sievely.lastSieve = ""

function loadedfolders(ftext) {
  console.log("folder response: " + ftext)
  var result = JSON.parse(ftext)
  if (result["result"] == "error") {
    if (Sievely.state != "LOGGED-OUT") {
      document.getElementById('navbar-login').classList.remove("sievely-hidden");
      document.getElementById('navbar-status').classList.add("sievely-hidden");
    }
    Sievely.state = "LOGGED-OUT";
    Sievely.allSievelyFolders = ["INBOX"]
    return;
  }
  if (Sievely.state != "LOGGED-IN") {
    document.getElementById('navbar-login').classList.add("sievely-hidden");
    document.getElementById('navbar-status').classList.remove("sievely-hidden");
  }
  Sievely.state = "LOGGED-IN";
  Sievely.allSievelyFolders = result["folders"]
}

function reloadfolders() {
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (x.readyState == 4 && x.status == 200) {
	loadedfolders(x.responseText)
      }
    }
    x.open("GET", "/1.0/folders", true);
    x.send(null);
}


function comment_to_xml(comment) {
  var lines = comment.split("\n");
  var collated = []
  for (var i = 0; i < lines.length; ++i) {
    if (lines[i].slice(0, 2) == "# ") {
      collated.push(lines[i].slice(2));
    } else {
      break;
    }
  }
  var comp = atob(collated.join(""));
  return pako.inflate(comp,{to:'string'});
}

Sievely.loadFromSieve = function(sievestr) {
  sievestr.replace(/\r\n/, "\n");
  if (sievestr.slice(0, Sievely.marker.length) != Sievely.marker) {
    /* Sieve does not start with a sievely marker, abort */
    console.log("Not a sievely sieve");
    return;
  }
  var xml = comment_to_xml(sievestr.slice(Sievely.marker.length));
  var dom = Blockly.Xml.textToDom(xml);
  Sievely.workspace.clear();
  Blockly.Xml.domToWorkspace(dom, Sievely.workspace);
}

function loadsieve() {
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (x.readyState == 4 && x.status == 200) {
	var json = JSON.parse(x.responseText);
	if (json['result'] != 'ok') {
	  console.log(json['error']);
	} else {
	  Sievely.loadFromSieve(json['sieve']);
	}
	var savebutton = document.getElementById('input-save');
	savebutton.classList.add('disabled');
	savebutton.innerHTML = "Saved";
	console.log("Save button set to saved on load");
	Sievely.lastSieve = Sievely.Generator.workspaceToCode(Sievely.workspace);
	savebutton.classList.remove('sievely-hidden');
	var loadbutton = document.getElementById('input-load');
	loadbutton.classList.remove('disabled');
	loadbutton.innerHTML = "Reload";
      }
    }
    x.open("GET", "/1.0/sieve/get", true);
    x.send(null);
}

Sievely.saveSieveToServer_ = function() {
  var sievestr = Sievely.Generator.workspaceToCode(Sievely.workspace)
  var x = new XMLHttpRequest();
  x.onreadystatechange = function() {
    if (x.readyState == 4 && x.status == 200) {
      var json = JSON.parse(x.responseText);
      var savebutton = document.getElementById('input-save');
      if (json['result'] != 'ok') {
	console.log("Failure saving: " + json['error']);
	savebutton.classList.remove('disabled');
	savebutton.innerHTML = "Save";
	console.log("Save button reset");
      } else {
	console.log("Saved to server OK:\n"+sievestr);
	savebutton.innerHTML = "Saved";
	Sievely.lastSieve = sievestr
      }
    }
  }
  x.open("PUT", "/1.0/sieve/put", true);
  //x.setRequestHeader("Content-Length", sievestr.length);
  x.setRequestHeader("Content-Type", "text/plain");
  x.send(sievestr);
}

Sievely.saveSieveToServer = function() {
  var savebutton = document.getElementById('input-save');
  if (savebutton.classList.contains('disabled')) {
    console.log("Not saving, button is disabled");
    return false;
  }
  savebutton.classList.add('disabled');
  savebutton.innerHTML = "Saving";
  console.log("Set button to saving...");
  Sievely.saveSieveToServer_(savebutton);
  return false;
}

Sievely.loadSieveFromServer = function() {
  var loadbutton = document.getElementById('input-load');
  if (loadbutton.classList.contains('disabled')) {
    console.log("Not loading, button is disabled");
    return false;
  }
  loadbutton.classList.add('disabled');
  loadbutton.innerHTML = "Loading";
  console.log("Set button to loading...");
  loadsieve();
  return false;
}

Sievely.enableSaveButton_ = function() {
  var sievestr = Sievely.Generator.workspaceToCode(Sievely.workspace)
  var savebutton = document.getElementById('input-save');
  if (sievestr == Sievely.lastSieve) {
    console.log("Sieve unchanged, not enabling save");
    savebutton.classList.add('disabled');
    savebutton.innerHTML = "Saved";
  } else {
    savebutton.classList.remove('disabled');
    savebutton.innerHTML = "Save";
    console.log("Enabled save button");
  }
}

Sievely.disableSaveButton_ = function() {
  var savebutton = document.getElementById('input-save');
  savebutton.classList.add('disabled');
  savebutton.innerHTML = "Saved";
  console.log("Enabled saved button");
}

Sievely.inject__ = function(toolboxxml, blocksxml) {
  var blocklyArea = document.getElementById('blocklyArea');
  var blocklyDiv = document.getElementById('blocklyDiv');
  var workspace = Blockly.inject(blocklyDiv,
				 {
				   toolbox: Blockly.Xml.textToDom(toolboxxml),
				   disable: false,
				   comments: false
				 });
  var onresize = function(e) {
    // Compute the absolute coordinates and dimensions of blocklyArea.
    var element = blocklyArea;
    var x = 0;
    var y = 0;
    do {
      x += element.offsetLeft;
      y += element.offsetTop;
      element = element.offsetParent;
    } while (element);
    // Position blocklyDiv over blocklyArea.
    blocklyDiv.style.left = (x + 4) + 'px';
    blocklyDiv.style.top = (y + 4) + 'px';
    blocklyDiv.style.width = (blocklyArea.offsetWidth - 8) + 'px';
    blocklyDiv.style.height = (blocklyArea.offsetHeight - 8) + 'px';
    Blockly.svgResize(workspace);
  };
  window.addEventListener('resize', onresize, false);
  onresize();
  Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(blocksxml),
			     workspace)
  workspace.addChangeListener(Blockly.Events.disableOrphans);
  workspace.addChangeListener(Sievely.enableSaveButton_);
  Sievely.workspace = workspace;
  reloadfolders();
  loadsieve();
}

Sievely.inject_ = function(toolboxxml) {
  var x = new XMLHttpRequest();
  x.onreadystatechange = function() {
    if (x.readyState == 4 && x.status == 200) {
      Sievely.inject__(toolboxxml, x.responseText)
    }
  }
  x.open("GET", "/sievely/startblocks.xml", true);
  x.send(null);
}

Sievely.inject = function() {
  var x = new XMLHttpRequest();
  x.onreadystatechange = function() {
    if (x.readyState == 4 && x.status == 200) {
      Sievely.inject_(x.responseText)
    }
  }
  x.open("GET", "/sievely/toolbox.xml", true);
  x.send(null);
}

Sievely.sanitiseSieveNumber = function(in_n) {
  // Sieve numbers are of the form nnnn{K,M,G}
  in_n = in_n.replace(/ /, "");
  var last = in_n.slice(-1).toUpperCase();
  if (last == "K" || last == "M" || last == "G") {
    in_n = in_n.slice(0, -1);
  } else {
    last = ""
  }
  in_n = Number(in_n) || 0;
  if (in_n < 0) { in_n = -in_n; }
  return in_n.toString() + last;
}
