/*
 * Sievely code generator
 * Copyright 2016 Daniel Silverstone <daniel.silverstone@codethink.co.uk>
 *
 * See COPYING
 */

'use strict';

goog.provide('Sievely.Generator')

goog.require('Blockly.Blocks')
goog.require('Sievely.Blocks')
goog.require('Blockly.Generator')

Sievely.Generator = new Blockly.Generator('Sieve');

Sievely.Generator.addReservedWords(
  "require,if,fileinto,stop,discard,reject"
);

Sievely.Generator.ORDER_ANY = 0;

function xml_to_comment(xml_) {
  var xmlb = btoa(pako.deflate(xml_,{to:'string'}));
  /* Encode it nicely */
  var xml_ = []
  while (xmlb.length > 60) {
    xml_.push("# " + xmlb.slice(0, 60) + "\n");
    xmlb = xmlb.slice(60);
  }
  xml_.push("# " + xmlb + "\n");
  return xml_.join("")
}

Sievely.Generator.init = function(workspace) {
  Sievely.Generator.extensions_ = Object.create(null);
  /* Generate the XML for the sievely script */
  var dom = Blockly.Xml.workspaceToDom(workspace);
  Sievely.Generator.xmlcomment_ = "# GENERATOR=SIEVELY\n" + xml_to_comment(Blockly.Xml.domToText(dom));
};

Sievely.Generator.finish = function(code) {
  var extensions = [];
  for (var extn in Sievely.Generator.extensions_) {
    extensions.push("require \"" + extn + "\";");
  }
  extensions = extensions.join("\n") + "\n\n";
  return Sievely.Generator.xmlcomment_ + "\n" + extensions + code;
};

Sievely.Generator.scrub_ = function(block, code) {
  var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
  var nextCode = Sievely.Generator.blockToCode(nextBlock);
  return code + nextCode;
};

Sievely.Generator.quote_ = function(string) {
  string = string.replace(/\\/g, '\\\\')
                 .replace(/\n/g, '\\\n')
                 .replace(/"/g, '\\"');
  return '"' + string + '"';
};

Sievely.Generator['sievely_start'] = function(block) {
  /* The 'Start' block generates a comment warning about sievely */
  return "/* Start of Sievely generated filter code */\n\n";
};

Sievely.Generator['sievely_fileinto'] = function(block) {
  Sievely.Generator.extensions_['fileinto'] = true;
  var mailboxname = Sievely.Generator.valueToCode(block, "MAILBOX",
						  Sievely.Generator.ORDER_ANY) || '"INBOX"'
  return "fileinto :create " + mailboxname + ";\n";
}

Sievely.Generator['sievely_filecopyinto'] = function(block) {
  Sievely.Generator.extensions_['fileinto'] = true;
  Sievely.Generator.extensions_['copy'] = true;
  var mailboxname = Sievely.Generator.valueToCode(block, "MAILBOX",
						  Sievely.Generator.ORDER_ANY) || '"INBOX"';
  return "fileinto :copy " + mailboxname + ";\n";
}

Sievely.Generator['sievely_listcheck'] = function(block) {
  var listname = Sievely.Generator.valueToCode(block, "LISTNAME",
					       Sievely.Generator.ORDER_ANY) || '"listname"';
  return ["header :contains \"List-Id\" " + listname, Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_isfrom'] = function(block) {
  var addr = Sievely.Generator.valueToCode(block, "ADDR",
					   Sievely.Generator.ORDER_ANY) || '"foo@bar"';
  return ["address :is :all \"From\" " + addr, Sievely.Generator.ORDER_ANY];
}


Sievely.Generator['sievely_envelope_detail'] = function(block) {
  Sievely.Generator.extensions_['subaddress'] = true;
  Sievely.Generator.extensions_['envelope'] = true;
  var detail = Sievely.Generator.valueToCode(block, "DETAIL",
					     Sievely.Generator.ORDER_ANY) || '"foo@bar"';
  return ["envelope :detail \"To\" " + detail, Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_header_test'] = function(block) {
  var header = Sievely.Generator.valueToCode(block, "HEADER",
					     Sievely.Generator.ORDER_ANY) || '"X-Sievely"';
  var content = Sievely.Generator.valueToCode(block, "CONTENT",
					      Sievely.Generator.ORDER_ANY) || '"unset"';
  var match = block.getFieldValue('MATCH');
  return ["header " + match + " " + header + " " + content,
	  Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_stop'] = function(block) {
  return "stop;\n";
}

Sievely.Generator['sievely_size'] = function(block) {
  var test = block.getFieldValue('TEST');
  var size = block.getFieldValue('SIZE');
  size = Sievely.sanitiseSieveNumber(size);
  return ["size " + test + " " + size, Sievely.Generator.ORDER_ANY];
}




Sievely.Generator['sievely_folder'] = function(block) {
  var val = block.getFieldValue('FOLDER');
  return [Sievely.Generator.quote_(val), Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_header'] = function(block) {
  var val = block.getFieldValue('HEADER');
  return [Sievely.Generator.quote_(val), Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['text'] = function(block) {
  var val = block.getFieldValue('TEXT');
  return [Sievely.Generator.quote_(val), Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_discard'] = function(block) {
  return "discard;\n";
}

Sievely.Generator['sievely_comment'] = function(block) {
  var val = block.getFieldValue('COMMENT');
  val = val.replace(/\*\//, "* /")
  return "\n/* " + val + " */\n\n";
}

Sievely.Generator['sievely_reject'] = function(block) {
  var reason = Sievely.Generator.valueToCode(block, "REASON",
					     Sievely.Generator.ORDER_ANY) || '"Don\'t like this message"'
  return "reject " + reason + ";\n";
}

Sievely.Generator['controls_if'] = function(block) {
  var n = 0;
  var argument = Sievely.Generator.valueToCode(block, 'IF' + n,
					       Sievely.Generator.ORDER_ANY) || 'false';
  var branch = Sievely.Generator.statementToCode(block, 'DO' + n);
  var code = 'if ' + argument + ' {\n' + branch;
  for (n = 1; n <= block.elseifCount_; n++) {
    argument = Sievely.Generator.valueToCode(block, 'IF' + n,
					     Sievely.Generator.ORDER_ANY) || 'false';
    branch = Sievely.Generator.statementToCode(block, 'DO' + n);
    code += '} elsif ' + argument + ' {\n' + branch;
  }
  if (block.elseCount_) {
    branch = Sievely.Generator.statementToCode(block, 'ELSE');
    code += '} else {\n' + branch;
  }
  return code + '}\n';
};

Sievely.Generator['sievely_group'] = function(block) {
  var kind = (block.getFieldValue('KIND') == "ALLOF") ? "allof" : "anyof";
  var conds = []
  for (var n = 0; n <= block.extras_ + 1; n++) {
    conds.push(Sievely.Generator.valueToCode(block, "COND" + n,
					     Sievely.Generator.ORDER_ANY) || 'false');
  }
  var expr = kind + " (" + conds.join(", ") + ")";
  return [expr, Sievely.Generator.ORDER_ANY];
}

Sievely.Generator['sievely_mark'] = function(block) {
  Sievely.Generator.extensions_['imap4flags'] = true;
  var mark = block.getFieldValue('MARK');
  var cmd = "addflag";
  if (mark[0] == '!') {
    cmd = "removeflag";
    mark = mark.slice(1);
  }
  return cmd + " \"\\\\" + mark + "\";\n";
}
