FROM debian:buster-slim
WORKDIR /src

ADD . /src/
RUN apt update && apt install -yq python-pip
RUN pip install -r /src/requirements.txt
ENTRYPOINT ["python", "sievely.py"]
