#!/usr/bin/python

from bottle import route, run, static_file, request, redirect, post, response, put

import sys
import imaplib
import re

sys.path.append("./lib")

from sievelib.managesieve import Client

ListResponse = re.compile(r'\((?P<flags>[^\)]*)\) "(?P<sep>.)" (?P<box>.*)')
ignore_flags = set(["\\Drafts", "\\Sent", "\\Trash", "\\Noselect"])

@route("/1.0/sieve/get")
def handle_sieve_get():
    cookie = request.get_cookie("sievely_data", default=None, secret="sievely")
    if (cookie is None) or len(cookie) == 0:
        return {"result": "error", "error": "No cookie"}
    c = Client(cookie.get('server', ''))
    if not c.connect(cookie.get('username',''),
                     cookie.get('password',''),
                     starttls=True):
        c.logout();
        return {"result": "error", "error": "Unable to log into sieve server"}
    scr = c.getscript('sievely')
    c.logout()
    if scr is None:
        return {"result": "error", "error": "No sieve stored in the server"}
    return {"result": "ok", "sieve": scr}

@put("/1.0/sieve/put")
def handle_sieve_put():
    cookie = request.get_cookie("sievely_data", default=None, secret="sievely")
    if (cookie is None) or len(cookie) == 0:
        return {"result": "error", "error": "No cookie"}
    c = Client(cookie.get('server', ''))
    if not c.connect(cookie.get('username',''),
                     cookie.get('password',''),
                     starttls=True):
        c.logout();
        return {"result": "error", "error": "Unable to log into sieve server"}
    sievestr = request.body.read()
    print("Sieve is:")
    print(sievestr)
    ok = c.putscript('sievely', sievestr)
    c.logout()
    if ok:
        return {"result": "ok"}
    return {"result": "error", "error": "unable to save sieve"}

@route("/1.0/folders")
def handle_folders():
    cookie = request.get_cookie("sievely_data", default=None, secret="sievely")
    if (cookie is None) or len(cookie) == 0:
        return {"result": "error", "error": "No cookie"}
    mail = imaplib.IMAP4_SSL(cookie['server'])
    ret = mail.login(cookie['username'], cookie['password'])
    print(repr(ret))
    if ret[0] != 'OK':
        mail.logout()
        return {"result": "error", "error": "Unable to login"}
    print("Ok, logged into IMAP")
    (ok, res) = mail.list()
    mail.logout()
    if ok != "OK":
        return {"result": "error", "error": "Unable to list"}
    mailboxes = []
    for entry in res:
        mo = ListResponse.match(entry)
        if not mo:
            return {"result": "error", "error": "Unable to parse list response"}
        else:
            flagset = set(mo.group('flags').split())
            sep = mo.group('sep')
            box = mo.group('box')
            if box[0] == '"':
                # Unquote mailbox name.  Is this enough?
                box = box[1:-1]
            if len(flagset.intersection(ignore_flags)) > 0:
                continue
            mailboxes.append(box)
    def cmpbox(a,b):
        if a == "INBOX":
            return -1;
        if b == "INBOX":
            return 1;
        return cmp(a, b)

    mailboxes.sort(cmp=cmpbox)
    
    return {"result": "ok", "folders": mailboxes }

@route("/google/<path:path>")
def handle_google(path):
    return static_file(path, "./google")

@route("/bootstrap/<path:path>")
def handle_google(path):
    return static_file(path, "./bootstrap")

@route("/test.html")
def handle_test():
    cookie = request.get_cookie("sievely_data", default=None, secret="sievely")
    if cookie is None:
        redirect("/")
    else:
        return static_file("test.html", ".")

@route("/")
@route("/index.html")
def handle_test():
    return static_file("index.html", "./templates")

@route("/sievely/<path:path>")
def handle_sievely(path):
    return static_file(path, "./sievely")

@route("/sievely.css")
def handle_css():
    return static_file("sievely.css", "./templates")

@post()
@route("/logout")
def logout():
    response.set_cookie('sievely_data', {}, secret="sievely")
    redirect("/")

@post()
@route("/login")
def login():
    username = request.params['username']
    password = request.params['password']
    server = request.params['server']
    # Have a go at verifying the provided credentials
    print("Testing Sieve credentials")
    c = Client(server)
    if not c.connect(username, password, starttls=True):
        redirect("/")
        return
    print("Ok, logged in to sieve")
    c.logout()
    mail = imaplib.IMAP4_SSL(server)
    ret = mail.login(username, password)
    print(repr(ret))
    if ret[0] != 'OK':
        redirect("/")
        return
    print("Ok, logged into IMAP")
    mail.logout()
    
    cookie_content = {"username":username,"password":password,"server":server}
    response.set_cookie("sievely_data", cookie_content, secret="sievely")
    redirect("/")

# And run the app

run(host='localhost', port=8080, debug=True, reloader=True)
